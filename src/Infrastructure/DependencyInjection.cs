﻿using System.Runtime.InteropServices;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Truyol.IrrigationSystem.Application.Common.Interfaces;
using Truyol.IrrigationSystem.Infrastructure.Persistance;
using Truyol.IrrigationSystem.Infrastructure.Services;

namespace Truyol.IrrigationSystem.Infrastructure
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddInfrastructure(this IServiceCollection services, IConfiguration configuration){
            services.AddDbContext<ApplicationDbContext>(options => options.UseNpgsql(configuration.GetConnectionString("default")));
            services.AddScoped<IApplicationDbContext>(provider => provider.GetService<ApplicationDbContext>()!);
            if(RuntimeInformation.ProcessArchitecture == Architecture.Arm || RuntimeInformation.ProcessArchitecture == Architecture.Arm64)
                services.AddTransient<IIrrigationCircuit, GpioIrrigationCircuit>();
            else {
                services.AddTransient<IIrrigationCircuit, DummyIrrigationCircuit>();
            }
            return services;
        }
    }
}
