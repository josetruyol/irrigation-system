using System.Device.Gpio;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Truyol.IrrigationSystem.Application.Common.Interfaces;

namespace Truyol.IrrigationSystem.Infrastructure.Services
{
    public class GpioIrrigationCircuit : IIrrigationCircuit
    {
        private readonly ILogger<GpioIrrigationCircuit> _logger;
        private readonly GpioController _gpioController;

        public int OutputPin { get; set; }
        public bool ActiveOnHigh { get; set; }

        public GpioIrrigationCircuit(ILogger<GpioIrrigationCircuit> logger)
        {
            _logger = logger;
            _gpioController = new GpioController();
        }

        public void Setup(int outputPin, bool activeOnHigh = true)
        {
            OutputPin = outputPin;
            ActiveOnHigh = activeOnHigh;
            if(!_gpioController.IsPinOpen(OutputPin) || _gpioController.GetPinMode(OutputPin) != PinMode.Output)
                _gpioController.OpenPin(OutputPin, PinMode.Output);
        }

        public async Task IrrigateAsync(int timeout)
        {
            _gpioController.Write(OutputPin, ActiveOnHigh ? PinValue.High : PinValue.Low);
            await Task.Delay(timeout * 1000);
            _gpioController.Write(OutputPin, ActiveOnHigh ? PinValue.High : PinValue.Low);
        }
    }
}