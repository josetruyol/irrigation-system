using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Truyol.IrrigationSystem.Application.Common.Interfaces;

namespace Truyol.IrrigationSystem.Infrastructure.Services
{
    public class DummyIrrigationCircuit : IIrrigationCircuit
    {
        private readonly ILogger _logger;
        public int OutputPin { get; set; }
        public bool ActiveOnHigh { get; set; }

        public DummyIrrigationCircuit(ILogger<DummyIrrigationCircuit> logger)
        {
            _logger = logger;
        }

        public void Setup(int outputPin, bool activeOnHigh = true)
        {
            OutputPin = outputPin;
            ActiveOnHigh = activeOnHigh;
        }

        public async Task IrrigateAsync(int timeout)
        {
            _logger.LogInformation($"Starting Irrigation on Pin {OutputPin}");
            await Task.Delay(timeout * 1000);
            _logger.LogInformation($"Stopping Irrigation on Pin {OutputPin}");
        }
    }
}