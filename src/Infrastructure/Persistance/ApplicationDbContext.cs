using System.Reflection;
using Microsoft.EntityFrameworkCore;
using Truyol.IrrigationSystem.Application.Common.Interfaces;

namespace Truyol.IrrigationSystem.Infrastructure.Persistance
{
    public class ApplicationDbContext : DbContext, IApplicationDbContext
    {
        public ApplicationDbContext(DbContextOptions options) : base(options) {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
            base.OnModelCreating(builder);
        }
    }
}