using System.Threading;
using System.Threading.Tasks;

namespace Truyol.IrrigationSystem.Application.Common.Interfaces
{
    public interface IApplicationDbContext
    {
        
        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
    }
}