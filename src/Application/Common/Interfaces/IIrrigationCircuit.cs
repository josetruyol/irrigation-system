using System.Threading.Tasks;

namespace Truyol.IrrigationSystem.Application.Common.Interfaces
{
    public interface IIrrigationCircuit
    {
        void Setup(int outputPin, bool activeOnHigh = true);
        Task IrrigateAsync(int timeout);
    }
}