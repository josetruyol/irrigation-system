using MediatR;

namespace Truyol.IrrigationSystem.Application.Irrigation.Queries.GetIrrigationStatus
{
    public class GetIrrigationStatus : IRequest<bool>
    {
        public int Circuit { get; set; }
    }
}
