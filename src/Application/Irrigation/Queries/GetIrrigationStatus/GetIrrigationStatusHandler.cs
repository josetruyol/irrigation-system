using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Truyol.IrrigationSystem.Application.Common.Interfaces;

namespace Truyol.IrrigationSystem.Application.Irrigation.Queries.GetIrrigationStatus
{
    public class GetIrrigationStatusHandler : IRequestHandler<GetIrrigationStatus, bool>
    {
        private readonly IIrrigationCircuit _irrigationSystem;

        public GetIrrigationStatusHandler(IIrrigationCircuit irrigationSystem)
        {
            _irrigationSystem = irrigationSystem;
        }
        public Task<bool> Handle(GetIrrigationStatus request, CancellationToken cancellationToken)
        {
            throw new global::System.NotImplementedException();
        }
    }
}