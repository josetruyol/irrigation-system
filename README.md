# Irrigation System

## Funcionalidades


### Sin red

Cuando no tenga conexion de red, funcionará con el reloj interno basado en el calendario. Actuando en periodos y tiempos activos dependiendo de en que temporada se encuentre (verano, primavera, otoño o invierno). 

Si posee termómetro usará la temperatura promedio para programar los riegos.

Todos los eventos quedarán registrados en una base de datos local hasta que tenga conexión de red y envie la informacion al servidor. Solo contará con 48 horas de datos guardados, para determinar temperaturas promedio en los ultimos dias

### Con red

Tomará las configuraciones de riego del servidor si esta disponible. El servidor puede activar riegos bajo demanda

### Sensores

Posibles sensores a integrar:
* Temperatura ambiental
* Humedad del suelo